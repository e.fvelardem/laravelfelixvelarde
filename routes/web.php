<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

//Products
//getAllProducts
Route::get('/products', [\App\Http\Controllers\ProductController::class, 'products'])->name('products');

//DeleteProduct
Route::get('/products/{product}/delete', [\App\Http\Controllers\ProductController::class, 'delete'])->name('products.delete');

//InsertProduct
Route::get('/products/create', function (){
    return view('create');
});
Route::post('/products/insert', [\App\Http\Controllers\ProductController::class, 'insert'])->name('products.insert');
//Products

//Clients
//getAllClients
Route::get('/clients', [\App\Http\Controllers\ClientsController::class, 'clients'])->name('clients');

//DeleteClient
Route::get('/clients/{clients}/delete', [\App\Http\Controllers\ClientsController::class, 'delete'])->name('clients.delete');

//InsertClient
Route::get('/clients/create', function (){
    return view('createclient');
});
Route::post('/clients/insert', [\App\Http\Controllers\ClientsController::class, 'insert'])->name('clients.insert');
//Clients

//Invoices
//CreateInvoices
Route::get('/invoice/create', function (){
    return view('invoiceinsertion');
});
Route::post('/invoice/insert', [App\Http\Controllers\ProductController::class, 'invoice'])->name('invoice.invoice');

//Invoices

//Suscriptions
//getSuscriptions
Route::get('/suscriptions', [App\Http\Controllers\SuscriptionController::class, 'suscription'])->name('suscriptions');

//buySuscription
Route::post('/suscriptions/buy', [App\Http\Controllers\SuscriptionController::class, 'buy'])->name('suscriptions.buy');
require __DIR__.'/auth.php';
