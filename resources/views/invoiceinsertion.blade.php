<div class="container">
    <h1>Creació de Invoices</h1><br>
    <form action="{{route('invoices.store')}}" method="POST">
        @csrf
        <div>
            <label for="client_id">Client:</label>
            <select name="client_id" id="client_id" required>
                @foreach($clients as $client)
                    <option value="{{$client->id}}">{{$client->name}}</option>
                @endforeach
            </select>
        </div>
        <br>
        <div>
            <h2>Productes:</h2>
            <br>
            @foreach($products as $product)
                <div>
                    <label>{{ $product->name }} ({{ $product->value }}€):</label>
                    <input type="number" name="products[{{ $product->id }}][quantity]" min="0" placeholder="Quantity">
                    <input type="hidden" name="products[{{ $product->id }}][price]" value="{{ $product->value }}">
                </div>
            @endforeach
        </div>
        <br>
        <div>
            <button type="submit">Crea!</button>
        </div>
    </form>
</div>
