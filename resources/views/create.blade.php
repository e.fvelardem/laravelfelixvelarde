 <!-- Be present above all else. - Naval Ravikant -->

 <div>
     <h1>Inserció De Productes</h1>
     <form action="{{route('products.insert')}}" method="post">
         @csrf
         <input type="text" id="name" name="name" placeholder="Name"><br/><br/>
         <input type="number" id="value" name="value" placeholder="Value"><br/><br/>
         <input type="number" id="quantity" name="quantity" placeholder="Quantity"><br/><br/>
         <input type="text" id="desc" name="desc" placeholder="Description"><br/><br/>
         <button type="submit">Crea!</button>
     </form>
 </div>
