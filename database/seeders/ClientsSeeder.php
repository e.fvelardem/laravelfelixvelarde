<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('clients')->insert([
           ['name' => 'Manolo', 'email' => 'manolo@manolo.com', 'phone' => '630912', 'address' => 'C.Carrer num.28'],
            ['name' => 'Ernesto', 'email' => 'ernesto@ernesto.com', 'phone' => '350213', 'address' => 'C.Carrer num.13'],
            ['name' => 'Pepe', 'email' => 'pepe@pepe.com', 'phone' => '790532', 'address' => 'C.Carrer num.1'],
            ['name' => 'Antonio', 'email' => 'antonio@antonio.com', 'phone' => '460612', 'address' => 'C.Carrer num.23'],
            ['name' => 'Juan', 'email' => 'juan@juan.com', 'phone' => '380917', 'address' => 'C.Carrer num.10']
        ]);
    }
}
