<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuscriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('suscriptions')->insert([
           ['type'=>'silver', 'value'=>100, 'description'=>'suscripció mensual, descompte per productes en %', 'discount'=>'10'],
            ['type'=>'gold', 'value'=>250, 'description'=>'suscripció mensual, descompte per productes en %', 'discount'=>'30'],
            ['type'=>'platinum', 'value'=>600, 'description'=>'suscripció mensual, descompte per productes en %', 'discount'=>'50']
        ]);
    }
}
