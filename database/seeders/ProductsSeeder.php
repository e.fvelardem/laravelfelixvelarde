<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('products')->insert([
           ['name'=>'healing potion', 'value'=>50, 'description'=>'cura 100HP'],
            ['name'=>'mana potion', 'value'=>70, 'description'=>'cura 100MP'],
            ['name'=>'hero shield', 'value'=>150, 'description'=>'para cualquier ataque']
        ]);
    }
}
