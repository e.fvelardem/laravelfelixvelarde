<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function products(){
        $products = Product::all();
        return view('products')->with('products', $products);
    }

    public function delete(Product $product){

        $product->delete();

        return redirect()->route('products')->with('status', 'Product deleted successfully');
    }
    public function insert(Request $request){
        $data = $request->validate([
           'name' => 'required|max:255',
            'value' => 'required',
            'quantity' => 'required',
            'desc' => 'required|max:255'
        ]);
        $product = new Product;
        $product->name = $data['name'];
        $product->value = $data['value'];
        $product->quantity = $data['quantity'];
        $product->description = $data['desc'];
        $product->save();
        return redirect()->route('products')->with('status', 'Product added successfully');
    }
    public function invoice(Request $request){
        $request->validate([
            'products.*.quantity' => 'required|integer|min:1',
            'products.*.price' => 'required|integer',
        ]);

        $clients_id = $request->input('client_id');
        $products = $request->input('products');
        var_dump($products);
        var_dump($clients_id);
        $invoice = Invoice::create([
            'clients_id' => $clients_id,
            'timestamp' => now(),
        ]);
        foreach ($products as $product_id => $detalls) {
            $product = Product::find($product_id);
            if ($product->quantity < $detalls['quantity']) { //Important xd
                return new Exception('Producte ' . $product->name . ' no té suficient stock');
            }
            $invoice->products()->attach($product_id, [
                'quantity' => $detalls['quantity'],
            ]);
            $product->quantity -= $detalls['quantity'];
            $product->save();
        }
        return redirect()->route('/invoice/create')->with('success', 'Factura creada amb exit');
    }
}
