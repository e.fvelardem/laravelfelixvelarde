<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Suscription;
use Illuminate\Http\Request;
use mysql_xdevapi\TableUpdate;

class SuscriptionController extends Controller
{
    public function suscriptions(){
        $suscriptions = Suscription::all();
        return view('suscriptions')->with('suscriptions', $suscriptions);
    }

    public function buy(Request $request){
        $data = $request->validate([
            'name' => 'nullable',
            'value' => 'required',
            'quantity' => 'nullable',
            'desc' => 'nullable'
        ]);
        $product = new Product;
        $product->name = $data['name'];
        $product->value = $data['value'];
        $product->quantity = $data['quantity'];
        $product->description = $data['desc'];
        $product->save();
        return redirect()->route('products')->with('status', 'Product added successfully');
    }
    public function delete(Product $product){
        $product->delete();
        return redirect()->route('products')->with('status', 'Product deleted successfully');
    }
}
