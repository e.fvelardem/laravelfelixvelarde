<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function clients(){
        $clients = Clients::all();
        return view('clients')->with('clients', $clients);
    }
    public function delete(Clients $clients){
        $clients->delete();
        return redirect()->route('clients')->with('status', 'Client deleted successfully');
    }
    public function insert(Request $request){
        $data = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'phone' => 'required',
            'address' => 'required|max:255'
        ]);
        $client = new Clients;
        $client->name = $data['name'];
        $client->email = $data['email'];
        $client->phone = $data['phone'];
        $client->address = $data['address'];
        $client->save();
        return redirect()->route('clients')->with('status', 'Client added successfully');
    }
}
