<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Clients extends Model
{
    public function invoice(): HasMany{
        return $this->hasMany(Invoice::class);
    }
    public function suscription(): HasOne{
        return $this->HasOne(Suscription::class);
    }
    use HasFactory;
}
